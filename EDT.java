public class EDT {
 private CharWindow w;
 protected Input input;
 protected Parser normalParser;
 protected Parser insertParser;
 protected Parser parser;
 protected Output out;

    /**
     * Define la lógica de la aplicación. Se usa un soporte para la información , la entrada y salida de esta y el parser para procesarla.
     * @author David Estebanell Redondo
     * @version 1
     *
     *
     */
 public EDT(){
 }
 public void setup(int width,int height,Input i,Output o){
     this.out=o;
     this.w=new CharWindow(width,height,' ',o);
     this.setupInput(i);
 }

    /**
     * Ejecutamos el tratamiento de caracteres.
     * @author David Estebanell Redondo
     *
     */
     public void run(){
     char c = input.get();
     while(c!=0){
     parser.exe(c);
     c=input.get();
     }
     this.print();
 }

    /**
     * Devuelve la ventana actual.
     * @author David Estebanell Redondo
     * @return Devuelve la ventana actual.
     */
 public CharWindow getWindow(){
    return this.w;
 }
 public void print(){
    this.out.print(this);
 }
 public void setupInput(Input i){
    this.input=i;
 }
 public void setupNormalParser(Parser normalParser){
    this.normalParser = normalParser;
    this.parser=this.normalParser;
 }
 public void setupInsertParser(Parser insertParser){
    this.insertParser = insertParser;
 }
 public void setNormalMode(){
     this.parser=normalParser;
 }
 public void setInsertMode(){
     this.parser=insertParser;
 }
}
