/**
 * En esta clase trataremos los vectores de información.
 * @author David Estebanell Redondo
 * @version 1
 *
 */

public class Array extends Data {
	protected Data[] content;
	protected int size;
	private Data def;
	public Array() {}

	/**
	 * Se encarga de generar el array.
	 * @author David Estebanell Redondo
	 *
	 * @param size Tamaño del array.
	 * @param def Contenido del array.
	 * @param o Salida de programa.
	 */
	public Array(int size, Data def, Output o) {
		this.content = new Data[size];
		this.setOutput(o);
		this.size = size;
		this.def = def;
		for (int i = 0; i < this.content.length; i++) {
			this.content[i] = this.def;
		}
	}

	/**
	 * Devuelve el tamaño.
	 * @author David Estebanell Redondo
	 * @return Devuelve el tamaño.
	 */
	public int getSize() {
		return this.size;
	}

	/**
	 * Ponemos un dato en una posición concreta del array.
	 * @author David Estebanell Redondo
	 *
	 * @param p Posición.
	 * @param v Dato.
	 */
	public void set(int p, Data v) {
		if (this.isLegal(p)) {
			this.content[p] = v;
		}
	}

	/**
	 * Nos devuelve el contenido de la posición.
	 * @author David Estebanell Redondo
	 * @param p Posición.
	 * @return Nos devuelve el contenido de la posición.
	 */
	public Data get(int p) {
		if (this.isLegal(p)) {
			return this.content[p];
		} else {
			System.out.println("Error, get");
			return new Char('x', this.out);
		}
	}

	/**
	 * Si la posición se encuentra en el array, ponemos el dato en dicha posicion.
	 * @author David Estebanell Redondo
	 *
	 * @param p Posición.
	 */
	public void rem(int p) {
		if (this.isLegal(p)) {
			this.set(p, this.def);
		}
	}

	/**
	 * devuelve el tamaño.
	 * @author David Estebanell Redondo
	 * @return devuelve el tamaño.
	 */
	public int length() {
		return content.length;
	}

	/**
	 * Devuelve si p se encuentra dentro o fuera del array.
	 * @author David Estebanell Redondo
	 * @param p Posición.
	 * @return Devuelve si p se encuentra dentro o fuera del array.
	 */
	public boolean isLegal(int p) {
		return (p >= 0 && p < this.size);
	}

	/**
	 * Inserta v en p.
	 * @author David Estebanell Redondo
	 *
	 * @param p Posición.
	 * @param v Dato.
	 */
	public void ins(int p, Data v) {
		if (this.isLegal(p)) {
			for (int i = this.size - 1; i > p; i--) {
				this.set(i, this.get(i - 1));
			}
			this.set(p, v);
		}
	}

	/**
	 * Borra el caracter de la posición indicada.
	 * @author David Estebanell Redondo
	 *
	 * @param p Posición.
	 */
	public void del(int p) {
		if (this.isLegal(p)) {
			for (int i = p; i < this.size - 1; i++) {
				this.set(i, this.get(i + 1));
			}
			this.rem(this.size - 1);
		}
	}

	public void ins(int p) {
		this.ins(p, this.def);
	}

	@Override
	public void print() {
		this.out.print(this);
	}

	protected void setDef(Data def) {
		this.def = def;
	}
}