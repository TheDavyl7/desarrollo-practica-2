/**
 * Procesa los comandos pasados y activa la opción correspondiente.
 * @author David Estebanell Redondo
 * @version 3
 *
 *
 */
public class NormalParser extends Parser{
    public NormalParser(EDT edt){
        super(edt);
    }

    /**
     * Se encarga de realizar distintas acciones sobre el EDT según la letra que lea.
     * @author David Estebanell Redondo
     *
     * @param c El caracter leído.
     */
    public void exe(char c){
        switch (c){
            case '0':
                this.edt.getWindow().first();
            break;

            case 'd':
                this.edt.getWindow().del();

            break;

            case 'G':
                this.edt.getWindow().setPosY(this.edt.getWindow().size);
            break;

            case 'g':
                this.edt.getWindow().setPosY(0);
                break;

            case 'h':
                this.edt.getWindow().prev();
            break;

            case 'i':
                this.edt.setInsertMode();
            break;

            case 'j':
                this.edt.getWindow().nextLine();
            break;

            case 'k':
                this.edt.getWindow().prevLine();
            break;

            case 'l':
                this.edt.getWindow().next();
            break;

            case 'o':
                this.edt.getWindow().ins();
                this.edt.getWindow().first();
                this.edt.setInsertMode();
            break;

            case 'x':
                this.edt.getWindow().delPos();
            break;

            case 'X':
                if(this.edt.getWindow().x != 0){
                    this.edt.getWindow().prev();
                    this.edt.getWindow().delPos();
                }
            break;

            case '$':
                this.edt.getWindow().last();
            break;
            }
        }
}
