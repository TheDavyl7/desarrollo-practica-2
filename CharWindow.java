/**
 * Extiende la clase Window incluyendo caracteres.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class CharWindow extends Window{
    public CharWindow(int width, int height, char def, Output o){
        super(width, height, new Char(def,o), o);
    }
    public void ins(char c){
        super.ins(new Char(c,this.out));
    }
}
