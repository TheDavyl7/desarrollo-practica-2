/**
 * Se encarga de recibir la información del sistema.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class Input {
    protected String s;
    private int p;
    public Input(String s){
        this.s=s;
        this.p=0;
    }

    /**
     * La R va recibiendo los caracteres del string hasta que no quede ninguno por leer.
     * @author David Estebanell Redondo
     *
     * @return Devuelve los caracteres que se le han pasado.
     */
    public char get(){
        char r = 0;
        if(p<s.length()) {
            r = s.charAt(p);
            this.p++;
        }
        return r;
    }
}
