/**
 * Se encarga de procesar los comandos.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
abstract public class Parser {
    protected EDT edt;
    public Parser(EDT edt){
        this.edt=edt;
    }
    public abstract void exe(char c);
}
