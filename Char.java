/**
 * Clase que se encarga de los caracteres que componen el texto.
 * @author David Estebanell Redondo
 * @version 1
 *
 */
public class Char extends Array {
 protected char content;
 public Char(char c,Output o){
	this.setOutput(o);
	this.set(c);
 }
 public void set(char c){
 	this.content=c;
 }
 @Override
 public void print() {
	this.out.print(this);
 }
}
