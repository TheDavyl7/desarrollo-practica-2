/**
 * Se encarga de representar la información por pantalla.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class Window extends Matrix {
    public int x;
    public int y;
    /**
     * Creamos la ventana.
     * @author David Estebanell Redondo
     *
     * @param width Anchura de la ventana.
     * @param height Altura de la ventana.
     * @param def Datos.
     * @param o Salida.
     */
    public Window(int width, int height, Data def, Output o){
        super(width,height,def,o);
        this.x=0;
        this.y=0;
    }

    /**
     * Ponemos el puntero en la posición X,Y.
     * @author David Estebanell Redondo
     *
     * @param x Columna.
     * @param y Fila.
     * @return Devuelve si la posición existe o no.
     */
    public boolean setPos(int x, int y){
        return this.setPosX(x)&&this.setPosY(y);
    }

    /**
     * Ponemos el puntero en la columna deseada.
     * @author David Estebanell Redondo
     *
     * @param x Columna
     * @return Si existe la posición o no.
     */
    public boolean setPosX(int x){
        if(this.isLegalX(x)){
            this.x=x;
            return true;
        }
        return false;
    }
    /**
     * Ponemos el puntero en la fila deseada.
     * @author David Estebanell Redondo
     *
     * @param y Fila
     * @return Si existe la posición o no.
     */
    public boolean setPosY(int y){
        if(this.isLegal(y)){
            this.y=y;
            return true;
        }
        return false;
    }

    /**
     * Si existe la posición o no.
     * @author David Estebanell Redondo
     * @param x Columna
     * @param y Fila
     * @return Si existe la posición o no.
     */
    public boolean isLegalPos(int x, int y){
        return this.isLegalX(x)&&this.isLegalY(y);
    }
    public boolean isLegalPos(){
        return this.isLegalPos(this.x, this.y);
    }

    /**
     * Si existe la posición o no.
     * @author David Estebanell Redondo
     * @param x Columna.
     * @return Si existe la posición o no.
     */
    public boolean isLegalX(int x){
        if(this.get(this.y).isLegal(x)){
            return true;
        }
        return false;
    }
    /**
     * Si existe la posición o no.
     * @author David Estebanell Redondo
     * @param y Fila.
     * @return Si existe la posición o no.
     */
    public boolean isLegalY(int y){
        if(super.isLegal(y)){
            return true;
        }
        return false;
    }

    /**
     * En la posición actual ponemos el carácter.
     * @author David Estebanell Redondo
     *
     * @param v Carácter.
     */
    public void ins(Data v){
        if (this.isLegalX(this.x)){
            super.ins(this.x, this.y, v);
            this.setPosX(this.x+1);
        }
    }

    public void ins() {
        this.nextLine();
        this.ins(this.y);
    }

    /**
     * Borramos la linea actual.
     * @author David Estebanell Redondo
     *
     */
    public void del(){
        this.del(this.y);
    }

    /**
     * Borramos la posicion actual.
     * @author David Estebanell Redondo
     *
     */
    public void delPos() {
        this.get(this.y).del(this.x);
    }

    /**
     * Devuelve la posición una posición más a la derecha.
     * @author David Estebanell Redondo
     * @return Devuelve la posición una posición más a la derecha.
     */
    public boolean next(){
        return this.setPosX(this.x+1);
    }
    /**
     * Devuelve la posición una posición más a la izquierda.
     * @author David Estebanell Redondo
     * @return Devuelve la posición una posición más a la izquierda.
     */
    public boolean prev(){
        return this.setPosX(this.x-1);
    }
    /**
     * Devuelve la posición una linea más abajo.
     * @author David Estebanell Redondo
     * @return Devuelve la posición una linea más abajo.
     */
    public boolean nextLine(){
        return this.setPosY(this.y+1);
    }
    /**
     * Devuelve la posición una linea más arriba.
     * @author David Estebanell Redondo
     * @return Devuelve la posición una linea más arriba.
     */
    public boolean prevLine(){
        return this.setPosY(this.y-1);
    }

    /**
     * Nos ponemos en la primera columna.
     * @author David Estebanell Redondo
     *
     */
    public void first(){
        this.setPosX(0);
    }
    /**
     * Nos ponemos en la última columna.
     * @author David Estebanell Redondo
     *
     */
    public void last(){
        this.setPosX(this.get(this.y).getSize()-1);
    }
    /**
     * Nos ponemos en la primera fila.
     * @author David Estebanell Redondo
     *
     */
    public void firstLine(){
        this.setPosY(0);
    }
    /**
     * Nos ponemos en la ultima fila.
     * @author David Estebanell Redondo
     *
     */
    public void lastLine(){
        this.setPosY(this.getSize()-1);
    }
    public void print(){
        this.out.print(this);
    }
}
