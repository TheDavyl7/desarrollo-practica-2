/**
 * Gestiona la matriz que gestiona el texto de cada ventana del EDT.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class Matrix extends Array {
	private int width;
	private Data def;

	/**
	 * Creamos la matriz.
	 * @author David Estebanell Redondo
	 *
	 * @param width Anchura de la matriz.
	 * @param height Altura de la matriz.
	 * @param def Datos.
	 * @param o Salida.
	 */
 public Matrix(int width, int height, Data def, Output o) {
	this.size=height;
	this.width=width;
	this.def=def;
	this.setOutput(o);
	this.content=new Data[height];
	this.setDef(new Array(width,def,this.out));
	for(int i=0;i<height;i++){
		this.content[i]=new Array(width,def,this.out);
	}
 }

	/**
	 * Añadimos en la posición Y el valor que tiene la array.
	 * @author David Estebanell Redondo
	 *
	 * @param y Fila.
	 */
 public void ins(int y){
 	this.ins(y, new Array(this.width,this.def,this.out));
 }

	/**
	 * En la posicion X,Y  ponemos el dato V.
	 * @author David Estebanell Redondo
	 *
	 * @param x Columna.
	 * @param y Fila.
	 * @param v Dato.
	 */
 public void ins(int x, int y, Data v){
	this.get(y).ins(x,v);
 }

	/**
	 * Borramos el dato que haya en la posición X,Y.
	 * @author David Estebanell Redondo
	 *
	 * @param x Columna.
	 * @param y Fila.
	 */
 public void del(int x,int y){
	this.get(y).del(x);
 }
 @Override
 public void print() {
	this.out.print(this);
 }
}
