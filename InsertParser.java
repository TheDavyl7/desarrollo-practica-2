/**
 * Se encarga de activar el metodo de inserción o el normal.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class InsertParser extends Parser {
    public InsertParser(EDT edt){
        super(edt);
    }

    /**
     * Se encarga de activar el modo normal o el de inserción según la letra que lea.
     * @author David Estebanell Redondo
     *
     * @param c El caracter leído.
     */
    public void exe(char c){
        if(c=='&'){
            this.edt.setNormalMode();
        }else{
            this.edt.getWindow().ins(c);
        }
    }
}
