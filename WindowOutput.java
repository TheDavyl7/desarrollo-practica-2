/**
 * Salida del sistema mediante Window.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class WindowOutput extends DecoratedOutput{
    public void print(Window w){
        System.out.println("["+w.x+"-"+w.y+"]");
        super.print((Matrix) w);
    }
}
