/**
 * Salida del sistema mediante el EDT.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class EDTOutput extends WindowOutput{
    public void print(EDT edt){
        System.out.print("EDT ");
        super.print(edt.getWindow());
    }
}
