/**
 * La clase Output nos permite aplicar distintas salidas a la información.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
abstract public class Output {
 /**
  * Se encarga de printar el caracter que se le pasa.
  * @author David Estebanell Redondo
  *
  * @param c Caracter pasado a printar.
  */
 public abstract void print(Char c);

 /**
  * Se encarga de printar el array que se le pasa.
  * @author David Estebanell Redondo
  *
  * @param a Array pasado a printar.
  */
 public abstract void print(Array a);

 /**
  * Se encarga de printar la matrix que se le pasa.
  * @author David Estebanell Redondo
  *
  * @param m Matrix pasada a printar.
  */
 public abstract void print(Matrix m);

 /**
  * Se encarga de printar la ventana que se le pasa.
  * @author David Estebanell Redondo
  *
  * @param w Ventana pasada a printar.
  */
 public abstract void print(Window w);

 /**
  * Se encarga de printar el texto procesado que se le pasa.
  * @author David Estebanell Redondo
  *
  * @param edt Texto procesado a printar.
  */
 public abstract void print(EDT edt);
 }
