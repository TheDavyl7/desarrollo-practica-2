import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Permite utilizar un archivo de texto como entrada al programa.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class FileInput extends Input{
    /**
     * Busca el archivo en la dirección escrita.
     * @author David Estebanell Redondo
     *
     * @param url Dirección del archivo.
     */
    public FileInput(String url) {
        super("");
        this.s=this.readFile(url);
    }

    /**
     * Lee el archivo y lo convierte en un string a tratar por el EDT.
     * @author David Estebanell Redondo
     *
     * @param url Dirección del archivo.
     * @return Devuelve el string a tratar.
     */
    private String readFile(String url){
        StringBuilder line = new StringBuilder();
        System.out.println(url);
        try {
            File file = new File(url);
            Scanner sc=new Scanner(file);    //file to be scanned
            while(sc.hasNextLine()) {
                //System.out.println(sc.nextLine());
                line.append(sc.nextLine());//returns the line that was skipped
            }
            sc.close();     //closes the scanner
        } catch(IOException e) {
            e.printStackTrace();
        }
        System.out.println(line.toString());
        return line.toString();
    }
}