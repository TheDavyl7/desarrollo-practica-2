/**
 * Decora la salida del sistema.
 * @author David Estebanell Redondo
 * @version 1
 *
 *
 */
public class DecoratedOutput extends Output{
    @Override
    public void print(Char c) {
        System.out.print(c.content);
    }
    @Override
    public void print(Array a) {

        for (int i = 0; i < a.length(); i++) {
            if(i==0){
                System.out.print("|");
            }
            if(i==a.length()-1){
                a.get(i).print();
                System.out.println("|");
            }
            else {
                a.get(i).print();
            }
        }
    }
    @Override
    public void print(Matrix m) {
        System.out.println(" ----------");
        for(int i=0;i<m.content.length;i++){
                m.content[i].print();
        }
        System.out.println(" ----------");
    }
    public void print(Window w){}
    public void print(EDT edt){}

}
